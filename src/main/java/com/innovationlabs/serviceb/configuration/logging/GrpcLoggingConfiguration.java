package com.innovationlabs.serviceb.configuration.logging;

import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor;
import org.springframework.context.annotation.Configuration;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-05
 * Time: 23:00
 */
@Configuration
public class GrpcLoggingConfiguration {

    @GrpcGlobalServerInterceptor
    GrpcLogInterceptor logInterceptor() {
        return new GrpcLogInterceptor();
    }
}

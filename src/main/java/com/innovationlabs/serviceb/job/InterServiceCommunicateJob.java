package com.innovationlabs.serviceb.job;

import com.innovationlabs.lib.InterServiceRequest;
import com.innovationlabs.serviceb.service.sender.InterServiceSender;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-06
 * Time: 01:30
 */
@Component
public class InterServiceCommunicateJob {

    private final InterServiceSender interServiceSender;

    @Setter
    @Value("${spring.application.name}")
    private String from;

    @Setter
    @Value("${interservice.ping.message}")
    private String message;

    public InterServiceCommunicateJob(InterServiceSender interServiceSender) {
        this.interServiceSender = interServiceSender;
    }

    /**
     * Sends a message to service-b using grpc.
     */
    @Scheduled(cron = "${interservice.ping.cron}")
    public void buzzServiceA() {
        var pingRequest = InterServiceRequest.newBuilder()
                .setFrom(from)
                .setMessage(message + " @ " + LocalDateTime.now())
                .build();

        interServiceSender.send(pingRequest);
    }
}

package com.innovationlabs.serviceb.service.sender;

import com.innovationlabs.lib.InterServiceReply;
import com.innovationlabs.lib.InterServiceRequest;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-06
 * Time: 00:31
 */
public interface InterServiceSender {
    InterServiceReply send(InterServiceRequest request);
}

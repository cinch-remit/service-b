package com.innovationlabs.serviceb.service.sender;

import com.innovationlabs.lib.InterServiceCommGrpc;
import com.innovationlabs.lib.InterServiceReply;
import com.innovationlabs.lib.InterServiceRequest;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-06
 * Time: 00:31
 */
@Service
@Slf4j
public class InterServiceSenderImpl implements InterServiceSender {

    @GrpcClient("service-a")
    private InterServiceCommGrpc.InterServiceCommBlockingStub interServiceCommBlockingStub;

    /**
     * Sends an interconnection request to service-a.
     * @param request The request to be sent to the server {@link InterServiceRequest}.
     * @return The response received from the server {@link InterServiceReply}
     */
    @Override
    public InterServiceReply send(InterServiceRequest request) {
        log.info("sending communication with message {} from {} to service-a", request.getMessage(), request.getFrom());
        var reply = interServiceCommBlockingStub.communicate(request);
        log.info("Got response {} from service-a", reply.getAllFields());

        return reply;
    }
}
